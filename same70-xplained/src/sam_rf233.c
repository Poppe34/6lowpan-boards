/************************************************************************************
 * configs/same70-xplained/src/sam_rf233.c
 *
 *   Copyright (C) 2015-2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <string.h>
#include <assert.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/irq.h>
#include <nuttx/arch.h>
#include <nuttx/fs/ioctl.h>
#include <nuttx/mtd/mtd.h>

#include "sam_gpio.h"
#include "sam_twihs.h"
#include "sam_spi.h"

#include "same70-xplained.h"
#include "chip/sam_pinmap.h"

#include <nuttx/wireless/ieee802154/at86rf23x.h>
#include <nuttx/wireless/ieee802154/ieee802154.h>
#include <nuttx/wireless/ieee802154/ieee802154_radio.h>

#include <nuttx/ieee802154/ieee802154_dev.h>


#ifdef HAVE_RADIO

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

#define SAMV7_EMAC0_DEVNAME "wpan0"

#define AT24XX_MACADDR_OFFSET 0x9a


#define GPIO_RF23X_IRQ_PIN 	(GPIO_ALTERNATE | GPIO_PORT_PIOD | GPIO_PIN28)
#define GPIO_RF23X_IRQ_INT	(GPIO_INPUT | GPIO_CFG_PULLUP | GPIO_CFG_DEGLITCH | \
				   GPIO_INT_FALLING | GPIO_RF23X_IRQ_PIN)
#define IRQ_RF23X_INT     	SAM_IRQ_PD28
#define GPIO_RF23X_SLPTR_PIN	(GPIO_ALTERNATE | GPIO_PORT_PIOC | GPIO_PIN17)
#define GPIO_RF23X_RESET_PIN	(GPIO_ALTERNATE | GPIO_PORT_PIOA | GPIO_PIN0)

/* Debug ********************************************************************/

/************************************************************************************
 * Private Data
 ************************************************************************************/

static int  sam_irq(FAR const struct at86rf23x_lower_s *lower, xcpt_t handler, int enable);
static void sam_slptr(FAR const struct at86rf23x_lower_s *lower, int state);
static void sam_reset(FAR const struct at86rf23x_lower_s *lower, int state);

struct at86rf23x_lower_s sam_lower = {
    .irq = sam_irq,
    .slptr = sam_slptr,
    .reset = sam_reset
};
/************************************************************************************
 * Private Functions
 ************************************************************************************/

 /************************************************************************************
  * Name: sam_irq
  *
  * Description:
  *   Handles setting the irq handler for the irq pin.  Also enables or
  *   disables the interrupt.
  *
  *  TODO: I need to check if the handler has be set before it can be enabled.
  *
  *
  ************************************************************************************/
static int sam_irq(FAR const struct at86rf23x_lower_s *lower, xcpt_t handler, int enable)
{
  irqstate_t flags;

  /* Disable interrupts until we are done.  This guarantees that the following
   * operations are atomic.
   */

  flags = enter_critical_section();

  /* Set the irq handler */

  if (handler != NULL)
    {

      /* Configure the interrupt */
      (void)irq_attach(IRQ_RF23X_INT, handler);

    }
  else
    {
      /* Detach and disable the interrupt */
      (void)irq_detach(GPIO_RF23X_IRQ_INT);

    }

  if(enable)
    {
      vdbg("Enable Radio IRQ\n");
      sam_gpioirqenable(IRQ_RF23X_INT);
    }
  else
    {
      vdbg("Disable Radio IRQ\n");
      sam_gpioirqdisable(IRQ_RF23X_INT);
    }
  leave_critical_section(flags);

  return OK;
}

/************************************************************************************
 * Name: sam_irq
 *
 * Description:
 *   Handles setting the irq handler for the irq pin.  Also enables or
 *   disables the interrupt.
 *
 *  TODO: I need to check if the handler has be set before it can be enabled.
 *
 *
 ************************************************************************************/
static void sam_reset(FAR const struct at86rf23x_lower_s *lower, int state)
{
  if(state)
    {
      sam_gpiowrite(GPIO_RF23X_RESET_PIN, 1);
    }
  else
    {
      sam_gpiowrite(GPIO_RF23X_RESET_PIN, 0);
    }
}

/************************************************************************************
 * Name: sam_irq
 *
 * Description:
 *   Handles setting the irq handler for the irq pin.  Also enables or
 *   disables the interrupt.
 *
 *  TODO: I need to check if the handler has be set before it can be enabled.
 *
 *
 ************************************************************************************/
static void sam_slptr(FAR const struct at86rf23x_lower_s *lower, int state)
{
  if(state)
    {
      sam_gpiowrite(GPIO_RF23X_SLPTR_PIN, 1);
    }
  else
    {
      sam_gpiowrite(GPIO_RF23X_SLPTR_PIN, 0);
    }
}


/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: sam_netinitialize
 *
 * Description:
 *   Configure board resources to support networking.
 *
 ************************************************************************************/
int weak_function sam_radioinitialize(void)
{
  FAR struct ieee802154_dev_s *ieee;
  FAR struct spi_dev_s *ieee_spi;

  /* Configure radio slp_tr */
  sam_configgpio(GPIO_RF23X_SLPTR_PIN);
  /* Configure radio reset */
  sam_configgpio(GPIO_RF23X_RESET_PIN);
  /* Configure radio irq */
  sam_gpioirq(GPIO_RF23X_IRQ_INT);

  ieee_spi = sam_spibus_initialize(SPI0_CS1);

  if(!ieee_spi)
    {
      dbg("ERROR: no spi interface\n");
      return ERROR;
    }
  ieee = at86rf23x_init(ieee_spi, &sam_lower);

  ieee802154_register(ieee, 0);

  return OK;
}

/************************************************************************************
 * Name: sam_wpan0_setmac
 *
 * Description:
 *   Read the Ethernet MAC address from the AT24 FLASH and configure the Ethernet
 *   driver with that address.
 *
 ************************************************************************************/

#ifdef HAVE_MACADDR
int sam_wpan0_setmac(void)
{
  struct i2c_master_s *i2c;
  struct mtd_dev_s *at24;
  uint8_t mac[6];
  ssize_t nread;
  int ret;

  /* Get an instance of the TWI0 interface */

  i2c = sam_i2cbus_initialize(0);
  if (!i2c)
    {
      ndbg("ERROR: Failed to initialize TWI0\n");
      return -ENODEV;
    }

  /* Initialize the AT24 driver */

  at24 = at24c_initialize(i2c);
  if (!at24)
    {
      ndbg("ERROR: Failed to initialize the AT24 driver\n");
      (void)sam_i2cbus_uninitialize(i2c);
      return -ENODEV;
    }

  /* Configure the AT24 to access the extended memory region */

  ret = at24->ioctl(at24, MTDIOC_EXTENDED, 1);
  if (ret < 0)
    {
      ndbg("ERROR: AT24 ioctl(MTDIOC_EXTENDED) failed: %d\n", ret);
      (void)sam_i2cbus_uninitialize(i2c);
      return ret;
    }

  /* Read the MAC address */

  nread = at24->read(at24, AT24XX_MACADDR_OFFSET, 6, mac);
  if (nread < 6)
    {
      ndbg("ERROR: AT24 read(AT24XX_MACADDR_OFFSET) failed: ld\n", (long)nread);
      (void)sam_i2cbus_uninitialize(i2c);
      return (int)nread;
    }

  /* Put the AT24 back in normal memory access mode */

  ret = at24->ioctl(at24, MTDIOC_EXTENDED, 0);
  if (ret < 0)
    {
      ndbg("ERROR: AT24 ioctl(MTDIOC_EXTENDED) failed: %d\n", ret);
    }

  /* Release the I2C instance.
   * REVISIT:  Need an interface to release the AT24 instance too
   */

  ret = sam_i2cbus_uninitialize(i2c);
  if (ret < 0)
    {
      ndbg("ERROR: Failed to release the I2C interface: %d\n", ret);
    }

  nvdbg("MAC: %02x:%02x:%02x:%02x:%02x:%02x\n",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  /* Now configure the EMAC driver to use this MAC address */

  ret = sam_emac_setmacaddr(EMAC0_INTF, mac);
  if (ret < 0)
    {
      ndbg("ERROR: Failed to set MAC address: %d\n", ret);
    }

  return ret;
}
#else
#  define sam_wpan0_setmac()
#endif

#endif
